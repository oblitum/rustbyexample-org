// In Rust, mutability of data structures is inherented
// by their components

struct point { x:f64, y:f64 }

fn main () {

    // The components of an immutable `point` can't be changed
    let p = point{ x:0.0, y:0.0 };
    //p.x = 10.0;

    // But for a mutable one, it's OK
    let mut p = point{ x:0.0, y:0.0 };
    p.x = 10.0;
    println!("{}", p.x);

    // Reference pointers can have mutability of the pointer,
    // as of the pointee, configured

    let x = 2;
    // a immutable pointer to a immutable pointee
    let r = &1;

    // not allowed
    //r = &x;
    //*r = 2;

    // A immutable pointer to a mutable pointee
    let r = &mut 1;

    // Not allowed
    //r = &x;

    // OK
    *r = 2;

    // A mutable pointer to a immutable pointee
    let mut r = &1;

    // OK
    r = &x;
    // Not allowed
    //*r = 2;

    let mut x = 2;

    // A mutable pointer to a mutable pointee
    let mut r = &mut 1;

    // OK
    r = &mut x;

    // OK
    *r = 2;

    // Unique pointers operates through mutability
    // inheritance: pointee inherents mutability
    // of the pointer

    let u = ~1;

    // not allowed
    //u = ~2;
    //*u = 2;

    let mut mu = ~1;
    mu = ~2;
    *mu = 2;

    // Turning an immutable box mutable through its
    // pointer
    mu = u;
    *mu = 3;
}
