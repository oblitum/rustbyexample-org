// Rust variables are _immutable_ (a.k.a. const) by default.

fn main() {

    // This would not compile, since `a` is immutable
    //let a = 1;
    //a = 2;

    // Use the `mut` keyword for declaring mutable variables
    let mut b = 1;
    println!("{}", b);
    b = 2;
    println!("{}", b);

    let c:int;

    // Since `c` is still uninitialized, this is allowed once
    c = 1;
    println!("{}", c);
    // This wouldn't compile since `c` is immutable and
    // already initialized
    //c = 2;
}
