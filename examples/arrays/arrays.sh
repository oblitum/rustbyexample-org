$ ./arrays
emp:[ 1 2 3 4 5 ]
dcl:[ 1 2 3 4 5 ]
set:[ 1 2 3 4 100 ]
get:100
len:5
[ 0 1 2 ] 
[ 1 2 3 ]

# There isn't much about static arrays. More about
# dynamical ones when we learn about pointers and
# ownership
