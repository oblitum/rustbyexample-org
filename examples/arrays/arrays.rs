// In Rust, an _array_ is a numbered sequence of a specific
// length of homogeneous elements

fn arr2str(a:&[int]) -> ~str {
    let mut s = ~"[";
    for i in a.iter() {
        s = s.append(format!(" {}", *i));
    }
    return s.append(" ]");
}

fn main() {

    // Here we create `a` of array type to hold exactly
    // 5 `int`s. The type of elements and length are both
    // part of the array's type. By default an array variable
    // doesn't refer to any array in memory until initialization.
    let a:[int, ..5];

    // This wouldn't compile
    //println!("emp:{}", arr2str(a));

    a = [1, 2, 3, 4, 5];
    println!("emp:{}", arr2str(a));

    // Use this syntax to declare and initialize a mutable
    // array in one line
    let mut b = [1, 2, 3, 4, 5];
    println!("dcl:{}", arr2str(b));

    // We can set a value at an index using the
    // `array[index] = value` syntax, and get a value with
    // `array[index]`
    b[4] = 100;
    println!("set:{}", arr2str(b));
    println!("get:{}", b[4]);

    // The `len()` function returns the length of an array
    println!("len:{}", b.len());

    // Array types are one-dimensional, but you can
    // compose types to build multi-dimensional data
    // structures
    let mut twoD:[[int, ..3], ..2] = [[0, ..3], ..2];
    for i in range(0, 2) {
        for j in range(0, 3) {
            twoD[i][j] = i + j;
        }
    }

    // Arrays are bound checked. If there was an out of bounds
    // access, we would get a proper out of bounds runtime
    // error, not a _possible_ access violation
    for l in twoD.iter() {
        println!("{} ", arr2str(*l));
    }
}
