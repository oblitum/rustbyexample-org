// Rust has various built-in types including strings,
// integers, floats, booleans, etc. Here are a few
// basic examples.

fn main() {

    // Strings, which can be added together with `+`.
    println!("{}", "rust" + "lang");

    // Integers and floats.
    println!("1+1 = {}", 1+1);
    println!("7.0/3.0 = {}", 7.0/3.0);

    // Booleans, with boolean operators as you'd expect.
    println!("{}", true && false);
    println!("{}", true || false);
    println!("{}", !true);
}
