// Branching with `if` and `else` in Rust is
// straight-forward.

fn main() {

    // Here's a basic example.
    if 7%2 == 0 {
        println!("7 is even");
    } else {
        println!("7 is odd");
    }

    // You can have an `if` statement without an else.
    if 8%4 == 0 {
        println!("8 is divisible by 4");
    }

    // There is no [ternary if](http://en.wikipedia.org/wiki/%3F:)
    // in Rust, so you’ll need to use a full `if` even for
    // basic conditions.
    // But in Rust several syntactic constructions are expressions,
    // they can assume values by omission of semicolons in the last
    // expression of their bodies
    let even = if 7%2 == 0 { "yes" } else { "no" };
    println!("is it even? {}", even);

    // Note that this wouldn't work because the absence of
    // `else` part is viewed as `else { () }` where `()` is a
    // _unit_ value of type `()`, incompatible with a string
    //let result = if condition { "yes" }

}

// Note that you don't need parentheses around conditions
// in Rust, but that the brackets are required.
