// Rust has no implicit reference types like Java or C#,
// in this sense, it's like C, you have value types,
// and you can have pointers (also being values) for them.

// The difference is that, except in an explicit _unsafe_
// context, all pointers carry ownership semantics with
// them, really preventing the programmer from doing the
// common memory access mistakes found in non-safe
// languages, without requiring a garbage collector.

use std::rc::Rc;

struct Point { x: f64, y: f64 }

fn main() {

    // This is the declaration of a pointer to int. This kind
    // of pointer works like a scoped view/reference into the
    // pointee. The pointer's lifespan must be shorter or equal
    // than that of the pointee (which avoids dangling references
    // and what not).
    let outer_r:&int;

    {
        let inner_r1:&int;

        let x:int;
        x = 1;

        // `&x` obtains the address of an object. Since both
        // pointee and pointer end their lifetimes at end of
        // scope, it's ok for this pointer to bind to this
        // pointee.
        inner_r1 = &x;

        // You can have several views into the same pointee
        let inner_r2 = inner_r1;

        // `*` is used for dereferencing pointers
        println!("{}, {}", *inner_r1, *inner_r2);

        // This wouldn't compile, since it would try to bind the
        // pointee to a reference with lifespan larger than that
        // of the pointee itself
        //outer_r = inner_r2;
    }

    // This also is a pointer declaration, but this type of pointer
    // operates in the semantics of an uniquely owned pointee
    let outer_u:~int;

    {
        let inner_u:~int;

        // This creates an object in a uniquely owned _box_, meaning
        // it's an object created on the heap to be managed by unique
        // pointers. `inner_u` starts as the owner of such a object.
        inner_u = ~2;

        // This fancy syntax would also work the same
        //inner_u = box 2;

        // The current owner's lifespan is shorter than the reference,
        // so, the uniquely owned object can't temporarily bind to such
        // reference
        //outer_r = &*inner_u;

        // This works, but given that binding an object in an uniquely
        // owned box to a reference, a.k.a. _borrowing_, would prevent
        // it from changing its owner until such reference dies,
        // we don't do it here so the next expressions works
        //let inner_r:&int;
        //inner_r = &*inner_u;

        // Pass the ownership of the object to an outer unique pointer,
        // `inner_u` is now unitialized, and can't be used anymore until
        // reinitialization
        outer_u = inner_u;

        // So, this wouldn't work
        //println!("{}", *inner_u);

        // `outer_u` and `outer_r` have the same lifespan, so this is
        // possible, the object is _borrowed_ by `outer_r` and now
        // it can't change its owner until it ceases to be borrowed
        outer_r = &*outer_u;
    }

    println!("{} {}", *outer_r, *outer_u);

    // This is a pointer to _reference counted_ object, for objects
    // that can have multiple owners. The object end its lifetime
    // as soon as there're no more reference counted pointers to it.
    let outer_rc:Rc<int>;

    {

        // This is how you create an reference counted object
        let inner_rc = Rc::new(3);

        // Assignment moves ownership instead of duplicating
        //outer_rc = inner_rc;

        // This yes, creates a new owner
        outer_rc = inner_rc.clone();

        // Now we can access the object both here
        println!("{}", *inner_rc);
    }

    // and here
    println!("{}", *outer_rc);

    // pointers have automatic dereferencing whe accessing data structure
    // elements
    let ps = &Point{ x:0.0, y:0.0 };
    println!("{} == {}", ps.x, (*ps).x);

    // pointers to array only work through automatic dereferencing
    let pa = &[1, 2, 3];
    let ua = ~[1, 2, 3];
    println!("{}", pa[0]);
    println!("{}", ua[0]);
}
