$ ./tuples
1, 2
1, 2
3, 2, 1
(3, 2, 1)

# Note: As Rust still doesn't provide numeric
# type parameters in traits, nor variadic
# parameters, there isn't facilities for
# ranging tuples and traits for generic access
# as with C++ `get<N>()`
