// A _tuple_ is a type built from a heterogeneous and
// ordered sequence of types. They're like structs but
// both tuple's type and its fields are unnamed.

fn main() {

    // A two element tuple, a.k.a. _pair_, provides the
    // special methods `first()` and `second()`
    let t = (1, 2.0);
    println!("{}, {}", t.first(), t.second());

    // But also provide the more general `n<N>()` methods
    println!("{}, {}", t.n0(), t.n1());

    // A mutable tuple can be set with a full tuple of same
    // type. More about setting specific elements in the
    // _Matching and Destructuration_ section
    let mut t = (1, 2.0, "3");
    t = (3, 2.0, "1");

    // There's no `first()` and `second()` for non-pairs
    //println!("{}, {}", t.first(), t.second());

    println!("{}, {}, {}", t.n0(), t.n1(), t.n2());
    println!("{}", t.to_str());
}
