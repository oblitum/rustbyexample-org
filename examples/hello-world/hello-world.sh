# To compile the program, put the code in `hello-world.rs`
# and use `rustc`.
$ rustc hello-world.rs

# It'll produce a binary.
$ ls
hello-world	hello-world.rs

# We can then execute the built binary.
$ ./hello-world
hello world

# Now that we can build and run basic Rust programs, let's
# learn more about the language.
