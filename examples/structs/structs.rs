// Rust's _structs_ are typed collections of fields.
// They're useful for grouping data together to form
// records.

// This `point` struct type has `x` and `y` fields
struct point { x: f64, y: f64 }

// Structs can also have anonymous fields. In this form
// it's known as _tuple structs_
struct complex(f64, f64);

fn main() {

    // This syntax creates a new struct. The fields must
    // be named at initialization.
    let s = point{ x: 1.0, y: 2.0 };

    // Access struct fields with a dot.
    println!("{} {}", s.x, s.y);

    // This syntax creates a new tuple struct
    let c = complex(42.0, 24.0);

    // This is how we access the anonymous fields of a
    // tuple struct, which we'll learn more about at the
    // _Matching and Destructuration_ section
    let complex(r, i) = c;
    println!("{} {}", r, i);
}
