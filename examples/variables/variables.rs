// In Rust, _variables_ are explicitly declared and used by
// the compiler to e.g. check type-correctness of function
// calls.

fn main() {

    // Use `let` to _declare_ variables
    // (`let` also does _binding_ which we'll
    // know more about later)
    let a:bool = true;
    println!("{}", a);

    // Rust can infer the type for variables.
    let b = "initial";
    println!("{}", b);

    // Declarations can hide previous declarations
    let a = 1;
    println!("{}", a);

    // Variables _declared_ without a corresponding
    // initialization start in _uninitialized state_.
    // For example, this use of uninitialized variable
    // wouldn't compile.
    //let c:int;
    //println!("{}", c);
}
