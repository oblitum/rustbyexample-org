fn main() {

    // `loop` provides a short syntax for infinite loops
    loop {
        println!("loop");
        break;
    }

    // The well known `while` loop
    let mut i = 1;
    while i < 3 {
        println!("{}", i);
        i += 1;
    }

    // there's no `for`, only `for-in` to range over
    // _iterators_
    for i in range(1, 3) {
        println!("{}", i);
    }

    // strings are UTF-8, characters may span more than
    // one byte, but you can properly traverse them using
    // `for-in`
    let s = "Hello, 世界";
    for c in s.chars() {
        if c > '\u0127' {
            print!("{}", c);
        }
    }

    println!("");
}
